

" input commands
imap <leader>ic \ic{}
nmap <leader>ic a \ic{}
imap EQU \begin{equation}<cr>\end{equation}<C-o>O
imap DIS \begin{displaymath}<cr>\end{displaymath}<C-o>O
imap IC \ic{}

imap TODO \todo{}
imap REQ \requirement{}<esc>la{}<esc>lld2f>hhi

"hard break (not working?)
"imap <C-Return> \\<cr>

" compiling
imap <leader>x <esc>:silent call Tex_RunLaTeX()<cr>
nmap <leader>x :silent call Tex_RunLaTeX()<cr>


" spell checking
" https://www.linux.com/learn/tutorials/357267:using-spell-checking-in-vim
" http://www.cs.swarthmore.edu/help/vim/vim7.html
setglobal spell spelllang=de,en
nmap <leader>y :call AddWordToSpellDictionary()<cr>



