
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Inspiration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" https://github.com/spf13/spf13-vim/blob/master/.vimrc
" http://www.cafepress.com/geekcheat.11507711
" http://www.cs.swarthmore.edu/help/vim/
" http://nvie.com/posts/how-i-boosted-my-vim/

" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
" http://statico.github.io/vim2.html
" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vundle Management
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" Inspiration: " https://github.com/vim-scripts?tab=repositories
" http://mirnazim.org/writings/vim-plugins-i-use/
" http://stackoverflow.com/questions/4777366/recommended-vim-plugins-for-javascript-coding 
" Bundle 'kien/ctrlp.vim'
" Bundle vim-abolish
" Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
" Bundle 'L9'
" Bundle 'FuzzyFinder'
" Bundle 'git://git.wincent.com/command-t.git'
" ...
":silent BundleInstall

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" color themes
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:rehash256 = 1

"256 colorthemes in terminal (requires vim-gnome)
"Bundle 'vim-scripts/CSApprox'

" use ColorScheme to set schemes that don't display correctly
"Bundle 'vim-scripts/colorsupport.vim'

"let g:ycm_server_keep_logfiles = 1
"let g:ycm_server_log_level = 'debug'

Bundle 'tomasr/molokai.git'
"colorscheme molokai
Bundle 'w0ng/vim-hybrid'
"colorscheme hybrid
Bundle 'zeis/vim-kolor'
"colorscheme kolor
Bundle 'jnurmine/Zenburn'
Bundle 'chriskempson/vim-tomorrow-theme'
colorscheme Tomorrow-Night-Eighties
Bundle 'marcus/vim-mustang'

Bundle 'reedes/vim-colors-pencil'
"colorscheme pencil
let g:airline_theme = 'pencil'
let g:pencil_higher_contrast_ui = 0   " 0=low (def), 1=high
set background=light






function! SetSolarized()
    set background=light
    "set background=dark
    let g:solarized_termcolors=256
    "colorscheme solarized
    ColorScheme solarized
endfunction



" BUGGY: slightly better when using custom function SetSolarized
Bundle 'altercation/vim-colors-solarized'
"Bundle 'vim-scripts/mayansmoke'
" Bundle 'vim-scripts/newspaper'
" Bundle 'jpo/vim-railscasts-theme'
" Bundle 'vim-scripts/paper'
" Bundle 'vim-scripts/pyte'
" Bundle 'gregsexton/Atom'

" scroll with <F8> <shift-F8> through all available color schemes + ability to ignore schemes!!
" (vim-misc is dependency of vim-colorscheme-switcher)
Bundle 'xolox/vim-misc'
Bundle 'xolox/vim-colorscheme-switcher'
" next/prev colorscheme with F8 / S-F8
:let g:colorscheme_switcher_exclude = ['blue', 'darkblue', 'default', 'delek',
            \ 'desert', 'elflord', 'evening', 'koehler', 'morning', 'murphy', 'pablo',
            \ 'peachpuff', 'ron', 'slate', 'Tomorrow-Night-Blue', 'torte', 'zellner']




"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" JS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"js/html indenting
"Bundle 'lukaszb/vim-web-indent'
Bundle "pangloss/vim-javascript"
" code-analysis engine for JavaScript (http://marijnhaverbeke.nl/blog/tern.html)
"Bundle 'marijnh/tern_for_vim'
" on-the-fly linting (BUGGY? glitiching text when moving around in command mode)
"Bundle 'wookiehangover/jshint.vim'

Bundle 'othree/javascript-libraries-syntax.vim'
let g:used_javascript_libs = 'underscore,angularjs'


Bundle 'mxw/vim-jsx'
let g:jsx_ext_required = 0

" restrict JSX to files with the pre-v0.12 @jsx React.DOM pragma:
"let g:jsx_pragma_required = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File Handling
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" recursive grep (!DEPENDENCY: sudo apt-get install ack-grep)
Bundle 'mileszs/ack.vim'

Bundle 'scrooloose/syntastic'
"let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_javascript_checkers = ['jsxhint']
" ruleset location for TNT
let g:syntastic_php_phpcs_args="--standard=~/tnt/build/ruleset.xml"


"let g:syntastic_phpcs_disable = 1
"Bundle 'fholgado/minibufexpl.vim'


" NERDTree and Toggle-NERDTree-width to adjust width to longest filename
Bundle 'scrooloose/nerdtree'
Bundle 'vim-scripts/Toggle-NERDTree-width'
map <leader>. :NERDTreeToggle<CR>
map <leader>e :NERDTreeToggle<CR>
let NERDTreeShowBookmarks=1
let NERDTreeIgnore = ['\.pyc$', '\.log$', '\.blg', '\.bbl', '\.aux', '\.out', '\.toc', '\.lof', '\.pdf', '\..\+\~', 'node_modules']
"let g:NERDTreeDirArrows=0

" Commenting
Bundle 'scrooloose/nerdcommenter'


"a complement to cli git
Bundle 'tpope/vim-fugitive'
"Bundle '/tpope/vim-sensible'

Bundle 'airblade/vim-gitgutter'

Bundle 'bling/vim-airline'
" Automatically displays all buffers when there's only one tab open.
let g:airline#extensions#tabline#enabled = 1
"let g:airline_powerline_fonts = 1

" highlight lines that have been changed from base version 
" show diff with <leader>d
"Bundle 'ghewgill/vim-scmdiff'

Bundle 'yegappan/grep'

" Full path fuzzy file, buffer, mru, tag, ...
Bundle 'kien/ctrlp.vim'
Bundle 'tacahiroy/ctrlp-funky'

let g:ctrlp_extensions = ['funky', 'line']

let g:ctrlp_funky_syntax_highlight = 1
nnoremap <Leader>fu :CtrlPFunky<Cr>
"map <leader><leader><Space> :CtrlPBuffer<CR>

" disable dotfile searching
" (src: https://github.com/kien/ctrlp.vim/issues/146)
let g:ctrlp_dotfiles = 0

"let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_cmd = 'CtrlPMixed'
"let g:ctrlp_by_filename = 1
let g:ctrlp_map = '<leader><leader><space>'
let g:ctrlp_root_markers = ['.ctrlp']

" fix for ctrlp actually using wildignore? (check :help ctrlp-options)
" (src: http://stackoverflow.com/questions/21017857/ctrlp-still-searches-the-ignored-directory)
if exists("g:ctrl_user_command")
  unlet g:ctrlp_user_command
endif

set wildignore+=*/.tmp/,*/tmp/*,*.so,*.swp,*.zip,*/node_modules/,bak.*
"let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_custom_ignore = {
    \ 'folders': '\v[\/](node_modules|target|dist)|(\.(swp|ico|git|svn))$'
\ }

"let g:ctrlp_custom_ignore = {
  "\ 'dir':  '\v[\/]\.(git|hg|svn)$',
  "\ 'file': '\v\.(exe|so|dll)$'
  "\ }




"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Text Navigation/Manipulation
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Navigation via tags (http://discontinuously.com/2011/03/vim-support-javascript-taglist-plus/)
Bundle 'int3/vim-taglist-plus'
"Bundle 'majutsushi/tagbar'
" Bundle 'vim-scripts/taglist.vim'
            \\

" undo tree with diff view
Bundle 'sjl/gundo.vim.git'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Bundle 'Lokaltog/vim-easymotion.git'
" movement commands on steroids
" https://github.com/Lokaltog/vim-easymotion/blob/master/doc/easymotion.txt
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
" keep cursor colum when JK motion
let g:EasyMotion_startofline = 0
" EasyMotion also uses the last keys to group targets
"let g:EasyMotion_keys = 'asdghklqwertyuiopzxcvbnmfj;'   " DEFAULT (; key bad for german keyboard)
let g:EasyMotion_keys = 'asdghklqwertyuiopzxcvbnmfj'   " DEFAULT (without ; key)
let g:EasyMotion_keys = 'asdghlqwertyuiopzxcvbnmf'   " DEFAULT (without ; j k  key)
" With this option set, v will match both v and V, but V will match V only
let g:EasyMotion_smartcase = 1
" Matching signs target keys by smartcase like. E.g. type '1' and it matches both '1' and '!' in Find motion
let g:EasyMotion_use_smartsign_jp = 1
" fix easymotion colors for vim in terminal
hi link EasyMotionTarget ErrorMsg
hi link EasyMotionShade  Comment

" bi-directional search (above & below the cursor)
nmap ff <Plug>(easymotion-s)
" replace vim's default search with easymotion search
map  / <Plug>(easymotion-sn)
"nmap  hh <Plug>(easymotion-sn)
"imap  hh <esc><Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
" These `n` & `N` mappings are options. You do not have to map `n` & `N` to EasyMotion.
" Without these mappings, `n` & `N` works fine. (These mappings just provide
" different highlight method and have some other features )
" map  n <Plug>(easymotion-next)
" map  N <Plug>(easymotion-prev)
" display, nagigate marks

" Gif config
"map ll <Plug>(easymotion-lineforward)
map jj <Plug>(easymotion-j)
map kk <Plug>(easymotion-k)
"map hh <Plug>(easymotion-linebackward)



"
"
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"Bundle 'kshenoy/vim-signature'


" Smooth animation of the cursor and the page whenever they move, with easing.
" nice, but sometimes weird behavior!
if v:version >= 703
    "Bundle 'joeytwiddle/sexy_scroller.vim.git'
endif

" align text at e.g. equal sign
Bundle "godlygeek/tabular"


" Show autocompletion while typing
"Bundle 'vim-scripts/AutoComplPop'
if v:version >= 703
    Bundle 'Valloric/YouCompleteMe'
endif

" automatic closing of quotes, parenthesis, brackets, etc.
Bundle 'Raimondi/delimitMate'

" snippets
Bundle 'SirVer/ultisnips'

" alternate between relative and absolute line numbers
"Bundle "myusuf3/numbers.vim"

" zen coding
Bundle "mattn/emmet-vim"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"vim-latex
Bundle 'git://vim-latex.git.sourceforge.net/gitroot/vim-latex/vim-latex'

"emacs-like yank-ring
Bundle 'vim-scripts/YankRing.vim'

" underlay the hexadecimal CSS colorcodes with their real color
Bundle 'ap/vim-css-color'

" extreme shell that doesn't depend on external shells
" but is written completely in pure Vim script
"Bundle 'Shougo/vimshell.vim'

" OpenGL Shader Language Highlighting
Bundle 'tikhomirov/vim-glsl'

" control google music from vim
Bundle 'bassnode/vim-google-play'

" highlight lines
"Bundle 'vim-scripts/highlight.vim'

" Mode-dependent status line highlighting (HOW TO SET UP?)
"Bundle 'Lokaltog/powerline'

" SublimeText-like multiple cursors (VERY SLOW!!!)
Bundle 'terryma/vim-multiple-cursors'
let g:multi_cursor_next_key='g<C-n>'
let g:multi_cursor_prev_key='g<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<esc>'


Bundle 'vim-scripts/Tab-Name'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VIMRC
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable filetype plugins
filetype plugin indent on

set autoread            " Set to auto read when a file is changed from the outside
set hidden              " hides buffers instead of closing them (you can have unwritten changes to a file and still open a new file)

set history=1000        " Store a ton of history (default is 20)
set autowrite           " automatically write a file when leaving a modified buffer

set timeoutlen=250      " delay before ambiguous mapping is triggered
"set ttimeoutlen=2050     " ???
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" User Interface
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"set number              " show line numbers
silent! set relativenumber      " Show the relative line number (vim >= 7.3)

set ignorecase          " Ignore cases when searching
set smartcase           " Case sensitive when mixed cases used in search

set hlsearch            " Highlight search results

set incsearch           " Makes search act like search in modern browsers

set showmatch           " Show matching brackets when text indicator is over them

set scrolloff=5         " Scrolling starts 5 lines before top/bottom of screen

"set scrollbind

" enables complete tab completion for command-mode and
" adds menu of all the possible matches when using it.
set wildmode=full wildmenu

set backspace=indent,eol,start   " allow backspacing over everything in insert mode

set visualbell          " don't beep
set noerrorbells        " don't beep

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" YouCompleteMe autocompletion optoins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:ycm_key_invoke_completion = '<C-S-Space>'
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/cpp/ycm/.ycm_extra_conf.py'
" jump to definition or declaration under cursor (use CTRL-o) to jump back to where command was invoked
nnoremap <leader>jd :YcmCompleter GoToDefinitionElseDeclaration<CR>

let g:ycm_complete_in_comments = 1
let g:ycm_complete_in_strings = 1                                    " default 1

let g:ycm_collect_identifiers_from_comments_and_strings = 1          " default 0
let g:ycm_collect_identifiers_from_tags_files = 1                    " default 0
let g:ycm_seed_identifiers_with_syntax = 1                           " default 0
let g:ycm_error_symbol = '>>'
let g:ycm_warning_symbol = '!'
" let g:UltiSnipsExpandTrigger="<c-j>"
" let g:UltiSnipsJumpForwardTrigger="<c-j>"
" autocmd FileType tex imap <c-j> <Plug>IMAP_JumpForward

let g:UltiSnipsJumpBackwardTrigger="<c-k>"

let g:ycm_add_preview_to_completeopt = 0
let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_always_populate_location_list = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Terminal specific
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set encoding=utf-8
set t_Co=256            " enable 256 colors

set <m-i>=i           " remap Alt-i (inserts \item in latex-suite) to work in terminal


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" globals
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader=','

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" buffer splits
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"nnoremap <c-h> <c-w>h
"nnoremap <c-j> <c-w>j
"nnoremap <c-k> <c-w>k
"nnoremap <c-l> <c-w>l

set splitright          " new split panes to right
set splitbelow          " new split panes to bottom

" DEFAULTS:
" Swap top/bottom or left/right split
"Ctrl+W R

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab           " Use spaces instead of tabs
set smarttab            " insert tabs on the start of a line according to shiftwidth, not tabstop

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set autoindent          "Auto indent
set smartindent         "Smart indent
"set wrap                "Wrap lines

nmap <leader>0 {=}''

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable           " Enable syntax highlighting



" Display extra whitespace
if v:version >= 703
    set list listchars=tab:»·,trail:·
endif

" character count indicator
" 1) highlight 80th column
" 2) highlight 120th column and onward
"highlight ColorColumn ctermbg=235 guibg=#2c2d27
"let &colorcolumn="80,".join(range(120,999),",")
"let &colorcolumn="80"


"Display matching parenthesis in bold wite text
:hi MatchParen cterm=bold ctermbg=none ctermfg=white



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => insert mode mappings
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
imap jk <esc>
imap kj <esc>

" DISABLED for better approach to save a file: <leader>s
"imap kj <esc>:update<cr>

imap AA <c-o>A
imap aa <c-o>a
imap II <c-o>I

imap <leader>a <esc>lea
imap <leader>B <esc>lBi
" normal -> insert: append after word
nmap <leader>a ea
nmap <leader>B Bi

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Movement Mappings
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" paragraph jumping
"nmap 7 {
"nmap 0 }

" switch window and maximize height (accordion effect);
nnoremap <c-h> <c-w>h<c-w>_
nnoremap <c-j> <c-w>j<c-w>_
nnoremap <c-k> <c-w>k<c-w>_
nnoremap <c-l> <c-w>l<c-w>_

nnoremap gh <c-w>h<c-w>_
nnoremap gj <c-w>j<c-w>_
nnoremap gk <c-w>k<c-w>_
nnoremap gl <c-w>l<c-w>_

" Wrapped lines goes down/up to next row, rather than next line in file.

"nnoremap j gj
"nnoremap k gk

"nnoremap hh <c-w>h<c-w>_
"nnoremap jj <c-w>j<c-w>_
"nnoremap kk <c-w>k<c-w>_
"nnoremap ll <c-w>l<c-w>_
nnoremap k gk


" move to begend of line
nmap <Space>4 $

" ##### vim-signature specific ####
" jump to next/previous mark
nmap <leader>j ]'
nmap <leader>k ['
"nmap <cr> ]'
"nmap OM ['

"nmap <leader>f /
nmap - /
vmap <C-a> ^
vmap <C-e> $

" Center to line after jumping to it
nnoremap G Gzz



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text Manipulation
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" capitalize word left of cursor
"imap jj <esc>~a
"imap jj <esc>"=nr2char(getchar())<cr>
"nmap <C-i> i_<esc>r

" avoid SHIFT key (i.e. moving pinky) for e.g. camel case
"   when in insert mode press jj followed by a lowercase letter
"   and the uppercase version will be inserted
imap jj <esc>:call InsertCapitalChar()<cr>
function! InsertCapitalChar()
    ":exec "normal i".toupper(nr2char(getchar())).""
    let thechar = toupper(nr2char(getchar()))
    :exec "normal a".thechar.""
    :call s:StartAppend()
endfunction

" The :startinsert command does not have an option for acting like "a"
" instead of "i" so this implements it.
" (src: https://github.com/vim-scripts/word_complete.vim/blob/master/plugin/word_complete.vim#L75)
fun! s:StartAppend()
  if strlen(getline(".")) > col(".")
    normal l
    startinsert
  else
    startinsert!
  endif
endfun

imap ## '

" remove search highlighting
nmap <leader>n :noh<cr>
imap <leader>n <esc>:noh<cr>

" append to end of line
nmap <Space>a A


function! PasteFromClipboard()
    set paste
    normal "+p
    set nopaste
endfunction

function! PasteFromClipboardAfter()
    set paste
    normal "+P
    set nopaste
endfunction

" paste from system clipboard
nmap <leader>p :call PasteFromClipboard()<cr>
imap <leader>p <esc>:call PasteFromClipboard()<cr>
nmap <leader>P :call PasteFromClipboardAfter()<cr>
imap <leader>P <esc>:call PasteFromClipboardAfter()<cr>

" replace word with yank butter
imap <leader>r <esc>viw"0p
nmap <leader>r viw"0p

" change next search match in current line
"nmap <leader>c V:s///e<cr>i

" search and replace all 'pattern' with 'replacement'
":LAck -l 'pattern' | xargs perl -pi -E 's/pattern/replacement/g'


" replace text til next uppercase letter
nmap <leader>c v/.\u<cr>d:nohlsearch<cr>i

" insert console log
au FileType javascript imap <buffer> cc console.log();<esc>hi

" search
nmap 77 /
nmap hh /

" jump to end of line
nmap 44 $

" append to end of line
nmap aa A

" indent inside curly bracket under cursor
nmap 55 =%

" find next braces/brackets/parenthesis and change inside
nmap cci) f)ci)
nmap cci9 f)ci)
nmap 99 f)ci)

nmap cci] f]ci]
nmap )) f]ci]

nmap cci} f}ci}
nmap cci0 f}ci}
nmap cci0 f}ci}
nmap 00 f}ci}
vmap 00 <esc>f}vi}

" cange after equal sign
nmap cc= ^f=llC
nmap cc0 ^f=llC

" insert semicolon a end of line
imap ;; <C-o>A;<esc>:w<cr>
"nmap ;; A;<esc>:w<cr>


" toggle Case of Character under cursor
nmap + v~

" correct last spelling mistake with first choice
nmap !! <Esc>[s1z=`]
nmap <leader>1 <Esc>[s1z=`]

" make header:
" =================================== HEADER ===================================
function! MakeHeader()
    call inputsave()
    let l:title = input('Enter name: ')
    call inputrestore()
    let l:len = strlen(b:title)
    let l:linelen = (80 - b:len)/2 - 2
    let l:line = repeat('=', b:linelen)
    let l:header = b:line . ' ' . b:title . ' ' . b:line
    call append(line('.'), b:header)
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text Manipulation
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" yank from the cursor to the end of the line, to be consistent with C and D.
nnoremap Y y$

" Press Shift+P while in visual mode to replace the selection without
" " overwriting the default register
vmap P p :call setreg('"', getreg('0')) <cr>

" Maps autocomplete to tab
"imap <Tab> <C-N>

"change the 'completeopt' option so that 
" Vim's popup menu doesn't select the first completion item, 
" but rather just inserts the longest common text of all matches; 
" and the menu will come up even if there's only one match.
" http://stackoverflow.com/questions/15996358/how-to-i-show-omnicompletes-autocomplete-list-without-autocompleting-current-te
":set completeopt=longest,menuone

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim-Command line mappings (emulate emacs)
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" prev word (alt-b)
cmap b <C-left>
" next word (alt-f)
cmap f <C-right>
" (buggy) delete word forward
cmap d <C-right><C-w>
" delete char forward
cmap <C-d> <right><BS>
cmap <C-h> <left>
cmap <C-l> <right>
cmap <C-a> <Home>
cmap <C-a> <Home>
cmap <C-b> <left>
cmap <C-f> <right>
cnoremap <C-k> <C-c>:
cmap kj <C-c>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => File Handling
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" copy current file path to system clipboard
nmap <leader>pwd :let @+" = expand("%:p")<cr>


" Change Working Directory to that of the current file
cmap cwd lcd %:p:h
cmap cd. lcd %:p:h

" Save current file if changed
imap <leader>s <esc>:update<cr>
nmap <leader>s :update<cr>
"nmap <Space><Space> :update<cr>
nmap <Space> :update<cr>

" For when you forget to sudo.. Really Write the file.
cmap w!! w !sudo tee % >/dev/null 

" persistent undo
"set undofile                " Save undo's after file closes
set undodir=~/.vim/undo     " where to save undo histories
"set undolevels=1000         " How many undos
"set undoreload=10000        " number of lines to save for undo

set backupdir=~/.vim/backup/
set directory=~/.vim/swap/

set history=1000            " remember more commands and search history"


" switch to previous buffer (ctrl-6)
nmap <leader><space> :e #<cr>

nmap <leader>b :b 

" also write file with :W (capital w)
cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))

nmap <leader>t :tabn<cr>
nmap tt :tabn<cr>

" open $MYVIMRC
nmap <leader><leader>rc :e $MYVIMRC<cr>
" source $MYVIMRC
nmap <leader><leader>so :so $MYVIMRC<cr>

" have Vim jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => MISC
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_CompileRule_pdf='pdflatex -interaction=nonstopmode $*'
" compile multiple times for cross-referencing
let g:Tex_MultipleCompileFormats='pdf'

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $* 

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'


au BufRead,BufNewFile *.less setfiletype css
au BufRead,BufNewFile *.ejs setfiletype html
au BufRead,BufNewFile *.frag setfiletype c
au BufRead,BufNewFile *.vert setfiletype c
"au BufWritePre * :normal gg=G'.
"au BufWritePre * :normal H=L'.

" delete buffer, leave split window intact
nmap <leader>q :b#<bar>bd#<CR> 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Abbrevations
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

ab clog console.log(
ab cl console.log(

" when accidentally hitting AltGr
imap æ a
imap ŧ t
imap ¶ r
imap ſ s
imap ł w



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => insert/normal mode distinction
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

 "---> filepath color depends on mode
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" first, enable status line always
set laststatus=2

" now set it up to change the status line based on mode
if version >= 700
  au InsertEnter * hi StatusLine term=reverse ctermbg=5 gui=undercurl guisp=Magenta
  au InsertLeave * hi StatusLine term=reverse ctermfg=0 ctermbg=2 gui=bold,reverse
endif

 "---> show cursor line only in insert mode
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" color of cursorline:
set cursorline
"hi CursorLine ctermbg=darkgray cterm=none
"hi CursorLine ctermbg=black cterm=none
"set cursorline
"autocmd InsertEnter,InsertLeave * set cul!



"""""""""""""""""""""""""""""""""""""""""""""""""""
" => JavaScript
"""""""""""""""""""""""""""""""""""""""""""""""""""
"nmap <leader>f :TernDef<cr>


" https://github.com/pangloss/vim-javascript
let g:html_indent_inctags = "html,body,head,tbody"
let g:html_indent_script1 = "inc"
let g:html_indent_style1 = "inc"

"""""""""""""""""""""""""""""""""""""""""""""""""""
" => Latex
"""""""""""""""""""""""""""""""""""""""""""""""""""
imap TD TODO
vmap ,td cTODO<esc>pi<c-j><esc>
imap ANN \annotation{}<esc>i
imap GRA \graveyard{}<esc>i

" insert hard line break at end of line
"imap OM <esc>A\\<esc>o
imap OM \\<cr>
"imap -> \arr 

" delete surrounding latex command such as \emph{my text}
nmap <leader><leader>s f\df{f}x



"""""""""""""""""""""""""""""""""""""""""""""""""""
" => MISC
"""""""""""""""""""""""""""""""""""""""""""""""""""
"nmap <F8> :TagbarToggle<CR>
let Tlist_Use_Right_Window = 1
let Tlist_WinWidth = 'auto'

nmap <leader>l :TlistToggle<cr>


nnoremap <silent> K bl#*:LAck <cword><CR>

":LAck -l 'pattern' | xargs perl -pi -E 's/pattern/replacement/g'

"function! Demo()
"    call inputsave()
"    let b:name = input('Enter name: ')
"    call inputrestore()
"    let b:pwd pwd
"    ":execute "LAck " . b:name .  ' . pwd
"endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Close Window
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use Q to intelligently close a window 
" (if there are multiple windows into the same buffer)
" or kill the buffer entirely if it's the last window looking into that buffer
function! CloseWindowOrKillBuffer()
  let number_of_windows_to_this_buffer = len(filter(range(1, winnr('$')), "winbufnr(v:val) == bufnr('%')"))
  " We should never bdelete a nerd tree
  if matchstr(expand("%"), 'NERD') == 'NERD'
    wincmd c
    return
  endif
  if number_of_windows_to_this_buffer > 1
    wincmd c
  else
    bdelete
  endif
endfunction
nnoremap <silent> Q :call CloseWindowOrKillBuffer()<CR>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Thesis
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
abb ROI Range Of Interest 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" uoe: CG
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"nmap <silent> <leader>g :! make run<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap ßß yiwea}<esc>bi\{<esc>

".vimrc file

"Markdown to HTML
nmap <leader>md :%!/opt/markdown/Markdown.pl --html4tags <cr>

" make beep:"
":set novisualbell
":set errorbells
":exe "normal \<Esc>"

" vim-google-play"
"let g:google_play_prefix = ",,"



" transform current buffer into nice looking JSON
nmap =j :%!python -m json.tool<CR>





" http://stackoverflow.com/questions/4390011/how-do-you-write-text-on-the-status-line-with-the-filename-row-and-col-number-w
"set statusline=%f%m%r%h\ [%L]\ [%{&ff}]\ %y%=[%p%%]\ [line:%05l,col:%02v]

" disable :w for saving
"cabbrev w :echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NOT <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Text inserts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
imap fnn function(<esc>mpa){<cr>}<esc>`pa
" insert paranthesis if 90 is typed quickly (only opening is needed, as closing will be inserted automatically)
imap 90 (

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colorscheme handling
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

command! -bar DayCS colorscheme Tomorrow


nmap <space>j }
nmap <space>k {
nmap <space>h ^
nmap <space>l $


