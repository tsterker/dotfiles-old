#!/bin/bash

########################################################################################
# GLEIF
########################################################################################
function leisearchpack(){
    #dest='lei-search-packed'
    dest=${1:-'lei-search-packed'}

    cd ~/Documents/GLEIF/sandbox
    cp -r lei-search $dest

    # delete everything but the dist folder
    cd $dest/bower_components/semantic-ui
    rm -r $(ls | grep -v dist)

    cd ~/Documents/GLEIF/sandbox
    echo resulting size:
    du -sh $dest
}

function leidataformatpack(){
    #dest='lei-search-packed'
    dest=${1:-'lei-data-format-packed'}

    cd ~/Documents/GLEIF/sandbox
    cp -r lei-data-format $dest

    cd ~/Documents/GLEIF/sandbox
    echo resulting size:
    du -sh $dest
}


########################################################################################
# DEFAULTS
########################################################################################

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
#alias ll='ls -alF'
alias ll='ls --color -lah --group-directories-first'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

########################################################################################
# CUSTOM
########################################################################################

# idms
#alias idmsssh="ssh -g -p 8081 apache@192.168.6.122 -L 8080:127.0.0.1:80"
alias idmsssh="ssh -g -p 8081 apache@192.168.7.116 -L 8080:127.0.0.1:80"
alias tnc="cd /home/apache/tnt_widgets/web/idms/widget/ChartWidget"
alias comp="cd /home/apache/tnt/src/IDMS/Component"
alias st="svn status"
alias int="cd /home/apache/tnt_widgets/web/idms/widget/ChartWidget/Component/InteractionManager/; vim InteractionManager.js"


# bash related
alias x="exit"
alias src="source ~/.bashrc"

# vim
function v()
{
    file=$1
    # open current directory if no parameter
    if [ -z $file ]; then
        file="."
    fi
    vim $file
}

# file conversion
function png2pdf()
{
    for f in *.png; do
      convert ./"$f" ./"${f%.png}.pdf"
    done
}

# misc
alias matrix="cmatrix -abs"

# goole translate (src: https://github.com/soimort/google-translate-cli)
# translate to english
alias tr="translate"
# translate to german
alias tg="translate {=de}"


# grep the output of ls -la (-i ignore case)
alias grepls="ls -la | grep -i $@"       
alias gls=grepls

# grep the output of man (-i ignore case)
function mangrep(){ man $1 | grep -i "${@:2}"; } 
alias gman=mangrep

# locations
alias ..="cd .."
alias cdd="cd $OLDPWD"
alias tmp="cd /tmp/ && mkdir -p tim && cd tim"
alias dot="cd ~/dotfiles"
alias bin="cd ~/.bin"
#alias ddf="cd /media/tster/DATA/#media/hoerbuecher/die-drei-fragezeichen_001-125/"
alias ddf="cmus"
alias tim="cd /media/tster/DATA/tim"

# files
alias b="$EDITOR ~/.bashrc && source ~/.bashrc"
alias a="$EDITOR ~/.bash/aliases && source ~/.bashrc"
alias f="$EDITOR ~/.bash/functions"

# MP3
function fixduration()
{
    mp3val $1 -f -t
}

# replace WHITESPACE with underscores
# in filenames under current directory 
function nowhitespace()
{
    find -name "* *" -type f | rename 's/ /_/g'
}

# edit scripts located in ~/.bin
function ebin()
{
    dir=~/.bin
    file="$1"

    # list all files if no parameter
    if [ -z $file ]; then
        for file in $(ls $dir); do
            # green if executable
            [ -x "$dir/$file" ] && igreenEcho "$file" || echo "$file"
        done
        return 0
    fi

    results=$(ls $dir | grep $file)
    count=$(ls $dir | grep -c $file)
    
    if [ "$count" -eq 0 ]; then
        redEcho "No file matching: '$file'. Available:"
        ebin   # recursive call
        return 1
    fi

    if [ "$count" -eq 1 ]; then
        file=$results
        whitePrompt "Edit file '${IGreen}$file${BWhite}'? <RET>"
        $EDITOR $dir/$file
        return 0
    fi

    declare -A choices
    i=1
    #echo "$results" | while read result; do
    for result in $results; do
        echo -ne "${Green}($i)${Color_Off} "
        echo $result | grep $file
        choices[$i]=$result
        ((i++))
    done 

    whitePrompt "Choose file to open: "

    $EDITOR $dir/${choices[$value]}
}

########################################################################################
# stuff
########################################################################################
function say()
{
    cd /home/tster/Desktop/
    rm -f nele:*
    text="$@"
    touch "nele: $text"
}

function xisio-less-watch()
{
    base=~/Documents/work/xisio/sandbox-charting/default
    watchFile=${base}/Templates/styles/less/*
    touchFile=${base}/Templates/styles/less/main.less
    prevDate=$(stat -c %Y $watchFile)


    while true; do
        changeDate=$(stat -c %Y ${watchFile})
        if [ "$prevDate" = "$changeDate" ]; then
            continue
        fi
        prevDate=$changeDate
        echo $(date +%T) "touched $touchFile"
        touch $touchFile
        sleep 1
    done
}

function xisio-less-compile()
{
    base=~/Documents/work/xisio/sandbox-charting/default
    node ${base}/less-watch-compiler.js ${base}/Templates/styles/less ${base}/Templates/styles/ &
}


########################################################################################
# git
########################################################################################
alias gid="git diff"
alias gis="git status --short"
alias giss="git status"
alias gia="git add"
alias gia.="git add ."
alias gic="git commit -am"
alias gicam="git commit --amend -am"
alias gip="whiteEcho 'pulling... '; git pull; whiteEcho 'pushing...'; git push"
alias gi="vim .gitignore"
alias ig="vim .gitignore"

# The ultimate graph display for git log (taken from http://bropages.org/)
alias gil="git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias gl="gil"
alias gil2="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias ss="git status --short;echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; gil;"

# compact git status
function s()
{
    branchName=$(git rev-parse --abbrev-ref HEAD);
    igreenEcho  '[' $branchName ']'

    status=$(git status --short)
    if [ -z "$status" ]; then
        whiteEcho "No Changes."
    else
        git status --short
    fi
}


# add commit pull push all in one
function g()
{
    commitMSG=$@

    if [ -z $commitMSG ]; then
        whiteEcho "Missing a commit message!"
        return
    fi

    #whitePrompt "Push current branch to remote master (including: add & commit, pull & push)? <RET>"
    #whitePrompt "Push current branch to remote master: '$commitMSG'? <RET>"
    whiteEcho 'Files to be added:'
    git status --short
    whitePrompt "Proceed pushing: '$commitMSG'? <RET>"
    git add .
    git commit -am "$commitMSG"
    whiteEcho 'pulling remote changes...'
    git pull
    whiteEcho 'pushing local changes...'
    git push -u origin master
}



function branchExists(){ git show-ref --verify --quiet refs/heads/"$1"; }
function currentBranch(){ git rev-parse --abbrev-ref HEAD; }
function isCurrentBranch() { [ "$(currentBranch)" == "$1" ]; }

function gib()
{
    # Usage:
    # gib               list branches
    # gib <branch>      switch to branch or create if not exists
    # gib <branch> !    delete branch

    source ~/.bash/colors

    branch="$1"
    optParam="$2"

    if [ -z $branch ]; then
        whiteEcho "/ usage: gib [branch] [!]"
        whiteEcho "\-------------------------/"
        git branch -a --color
    elif ! branchExists $branch; then
        redEcho "Branch $branch does not exist."
        whitePrompt "Want to create it? <RET>"
        git checkout -b $branch
    elif [ "$optParam" == "!" ]; then

        if isCurrentBranch $branch; then
            redEcho "Can not delete branch you are currently on."
            whitePrompt "Switch to branch master and delete '$branch'?"
            git checkout master
        else
            whitePrompt "Delete branch '$branch'? <RET>"
        fi
        git branch -d $branch
    else
        git checkout $branch
    fi
}

# manage vagrant vms
function vms()
{
    command=$1

    if [ -z $command ]; then
        VBoxManage list runningvms
    elif [ $command == "halt" ]; then
        # grab vm UUIDs
        UUIDs=$(VBoxManage list runningvms | sed -e 's/.*{\(.*\)}/\1/g')
        echo $UUIDs
    fi
}

# added by mkalias
alias dat="cd /media/tster/DATA"
alias down="cd /home/tster/Downloads"
alias install="sudo apt-get install"
alias master="cd /media/tster/DATA/tim/master"
alias aic="alertOnInternetConnection"
alias aoc="alertOnInternetConnection"

alias update="sudo apt-get update"
alias thesis="cd /media/tster/DATA/hsrm/thesis"
alias sem6="cd /media/tster/DATA/hsrm/semester6"
alias phil="cd /media/tster/DATA/tim/philipp"
alias pyserver="python -m SimpleHTTPServer 8080"
alias pyserve="python -m SimpleHTTPServer"
alias mendeley="/home/tster/Desktop/mendeleydesktop-1.8.4-linux-i486/bin/mendeleydesktop"
alias aus="cd /home/tster/Documents/bachelor-thesis/ausarbeitung"
alias aus!="cd /home/tster/Documents/bachelor-thesis/ausarbeitung; vim main.tex"
alias ch!="cd /home/tster/Documents/bachelor-thesis/ChartPlotter/js; vim demo.js"
alias chs="cd /home/tster/Documents/bachelor-thesis/ChartPlotter; pyserver"
alias enw="emacs -nw"
alias t="tree"
alias th="cd /home/tster/Documents/bachelor-thesis"
alias md="mkcd"
alias jslines="cloc . --exclude-dir=lib"
alias humansize="du -sh"
alias diskusage="df -h /dev/sd*"
alias lss="ls -lash | sort -k1,1 | grep -E ^[0-9]+,[0-9]+"
alias paste="cnp paste"
alias uoe="cd /home/tster/Documents/uoe"
alias ssh-uoe="ssh s1365873@student.ssh.inf.ed.ac.uk"
alias weka="java -jar /home/tster/Documents/uoe/semester1/iaml/weka-3-6-2/weka.jar"
alias sem1="cd /home/tster/Documents/uoe/semester1"
alias cg="cd /home/tster/Documents/uoe/semester1/cg/assignments/assignment2/answers/"
alias go="cd /home/tster/Documents/uoe/semester1/cg/assignments/assignment2/answers/code; make run"
alias afs="cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop"
alias startafs="sudo /etc/init.d/openafs-client start"
alias iaml="cd /home/tster/Documents/uoe/semester1/iaml/assignments/assignment4"
alias dice="kinit s1365873; aklog;cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop"
alias diceks="kinit s1050075; aklog;cd /afs/inf.ed.ac.uk/user/s10/s1050075/Desktop"
alias cdice="cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop"
alias dicecg="cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop/cg/assignment1/"
alias dice-cg="cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop/cg/assignment1/"
alias diceiaml="cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop/iaml/assignment2"
alias dicemlpr="cd /afs/inf.ed.ac.uk/user/s13/s1365873/Desktop/mlpr/"
alias idms="cd /home/tster/Documents/idms/"
alias doc="cd /home/tster/Documents"
alias bib="cd /home/tster/Documents/latex-bibs"
alias exc="cd /home/tster/Documents/uoe/semester1/exc/assignments/assignment1"

# after logging in do:
# ssh student.ssh.inf.ed.ac.uk
# ssh student.login
alias sshuoe="ssh s1365873@student.ssh.inf.ed.ac.uk"

alias sem1="cd /home/tster/Documents/uoe/semester1"
alias mlpr="cd /home/tster/Documents/uoe/semester1/mlpr/assignments/assignment1"
alias vim.="vim ."
alias irr="cd /home/tster/Documents/uoe/semester1/irr"
alias tim="cd /home/tster/Documents/tim/"
alias nele="cd /home/tster/Documents/people/nele"
alias cav="cd /home/tster/Documents/uoe/semester2/cav"
alias dme="cd /home/tster/Documents/uoe/semester2/dme"
alias pmr="cd /home/tster/Documents/uoe/semester2/pmr"
alias rl="cd /home/tster/Documents/uoe/semester2/rl"
alias cav="cd /home/tster/Documents/uoe/semester2/cav/coursework/a1/project"
alias oi="notify-send HEY"
alias xxx="mv lol-* lol-yo"
alias nele="ssh nele@192.168.1.4"
alias desk="cd; cd Desktop"
alias sem2="cd /home/tster/Documents/uoe/semester2"
alias irp="cd /home/tster/Documents/uoe/msc-project/irp"
alias py="python"
alias rl="cd /home/tster/Documents/uoe/semester2/rl/assignments/a1"
alias dme="cd /home/tster/Documents/uoe/semester2/dme/project"
#alias touch="cd /home/tster/Documents/idms/touch-sandbox"
alias dme="cd /home/tster/Documents/uoe/semester2/dme/dmeproject"
alias fixsound="sudo alsa force-reload"
alias idmsvpn="cd /home/tster/Documents/idms/vpn/; sudo openvpn te805343_IS-vpn.ovpn"
alias cav="cd /home/tster/Documents/uoe/semester2/cav/coursework/a2/project"
alias clearswp="rm .*.swp"
alias rl="cd /home/tster/Documents/uoe/semester2/rl/assignments/a2/project"
alias sand="cd /home/tster/Documents/uoe/msc-project/sandbox/testProject"
alias sl="sails lift"
alias sand="cd /home/tster/Documents/uoe/msc-project/sandbox/sql-test"
alias sand="cd /home/tster/Documents/uoe/msc-project/sandbox/test"
alias sll="rm -rf .tmp/; sl"
alias msc="cd /home/tster/Documents/uoe/msc-project"
alias beta="cd /home/tster/Documents/uoe/msc-project/sandbox/beta"
alias msc="cd ~/Documents/uoe/msc-project/project/dissertation/; vim main.tex"
alias bachelor="evince ~/Documents/hsrm/bachelor/bachelor-thesis-final/ausarbeitung/main.pdf"
alias proto="cd /home/tster/Documents/uoe/msc-project/feature-prototypes"
alias code="cd /home/tster/Documents/uoe/msc-project/project/code"
alias proj="cd /home/tster/Documents/uoe/msc-project/project/CollaQuiz"
alias p="cd /home/tster/Documents/uoe/msc-project/project/CollaQuiz"
alias smodel="sails generate model"
alias scontroller="sails generate controller"
alias sl!="rm -rf .tmp/ && sl"
alias vs="cd ~/Documents/uoe/msc-project/project/CollaQuiz/api/; v"
alias vc="cd ~/Documents/uoe/msc-project/project/CollaQuiz/assets; v"
alias tnt="cd /home/apache/tnt"
alias texbak="mv main.tex xmain.tex"
alias texunbak="mv xmain.tex main.tex; touch main.latexmain"
alias texfresh="texbak; rm main.*; texunbak"
alias freshtex="texbak; rm main.*; texunbak"
alias gleif="cd /home/tster/Documents/GLEIF"
alias sitemap="cd /home/tster/Documents/GLEIF/sandbox/sitemap-app"
alias vagup="vagrant up"
alias vaghalt="vagrant halt"
alias sshinka="ssh inkadev@inkadev.xisio.com"
alias sshinkaX="ssh -xC inkadev@inkadev.xisio.com"
alias sshinkaLive="ssh -xC inka4neo@inkadev.xisio.com"
alias violet="java -jar violetumleditor-2.0.1.jar"
alias violet="java -jar /home/tster/Desktop/violetumleditor-2.0.1.jar"
alias upxisio="cd ~/Documents/work/xisio/sandbox/RntvEP; vagrant up; cd .."
alias xisio-halt="cd ~/Documents/work/xisio/sandbox-charting/MlaYKe; vagrant halt; cd .."
alias xisio-up="cd ~/Documents/work/xisio/sandbox-charting/MlaYKe; vagrant up; cd .."
alias xisio="cd /home/tster/Documents/work/xisio/sandbox-charting/default"
alias xlc="xisio-less-compile"
alias xlw="xisio-less-watch"
alias gleif-map="cd /home/tster/Documents/GLEIF/sandbox/woldmap/v1"
alias vup="vagrant up"
alias valt="vagrant halt"
alias xxx="cd ~/Documents/work/xisio/sandbox-charting/default/Templates/styles/less; while true; do lessc main.less > ../main.css; echo 'compiled'; sleep 1; done;"
alias lei-search="cd /home/tster/Documents/GLEIF/sandbox/lei-search"
alias leisearch="cd /home/tster/Documents/GLEIF/sandbox/lei-search"
alias leidataformat="cd /home/tster/Documents/GLEIF/sandbox/lei-data-format"
alias lserver="live-server"
alias liserver="live-server"
alias gleif.dev="cd ~/Documents/GLEIF/sandbox/website/ZEhlhL; vagrant up; cd ../gleif-website"
alias gleif.dev!="cd ~/Documents/GLEIF/sandbox/website/ZEhlhL; vagrant halt"
alias cdgleif.dev="cd /home/tster/Documents/GLEIF/sandbox/website/gleif-website"
alias sshhosteurope="ssh wp12272923@vwp14881.webpack.hosteurope.de"
alias rbt="rm -r /home/tster/Documents/GLEIF/sandbox/website/gleif-website/assets/bowerTest/"
alias ccat="pygmentize -g"
