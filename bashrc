# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

##################################
# add scripts to path
##################################
PATH=$PATH:~/.bin

# https://github.com/magicmonty/bash-git-prompt
#source ~/.bash/gitprompt.sh

##################################
# prompt 
##################################
PS1="\[\033[32m\][\w]\[\033[0m\]\n\`if [[ \$? = "0" ]]; then echo "\\[\\033[36m\\]"; else echo "\\[\\033[31m\\]"; fi\`(\j)\u\[\033[1;33m\]-> \[\033[0m\]"


##################################
# source files
##################################
bashdir=~/.bash
sourcefiles="config aliases functions colors"
for file in $sourcefiles; do
    if [ -f $bashdir/$file ]; then
        source $bashdir/$file
    fi
done
unset bashdir
unset sourcefiles

export TERM=xterm-256color

# http://perlgeek.de/en/article/set-up-a-clean-utf8-environment
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

alias sn="sails lift"
