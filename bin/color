#!/bin/bash

VERSION="v1.0-30042012"
COLOR_CODE=0	# Default BLACK

# Mode Code
# Intense, Background, ...
MODE_CODE=0

# Type Code
# Bold or Underlined, ...
TYPE_CODE=0

# argument patterns
PATTERN_REGULAR="\-r|--regular"
PATTERN_BOLD="\-b|--bold"
PATTERN_CONTINUOUS="\-c|--continuous"
PATTERN_UNDERLINED="\-u|--underlined"
PATTERN_INTENSE="\-i|--intense"
PATTERN_BACKGROUND="\-B|--background"

# argument bits
REGULAR=1
BOLD=0
UNDERLINED=0
INTENSE=0
BACKGROUND=0



function print_help {
	echo "Usage:"
	echo "   color {color} [mode]"
	echo ""
	echo " color:"
	echo "   red|green|blue|purple|yellow|purple|cyan|white"
	echo " mode:"
	echo "   bold|underlined|regular|background"
	echo ""
	echo " -b, --bold"
	echo "     Prints the text bold"
	echo ""
	echo " -u, --underlined"
	echo "     Underlines the text"
	echo ""
	echo " -r, --regular"
	echo "     Prints text without any glitter and glamour"
	echo ""
	echo " -B, --background"
	echo "     Sets the color as background"
	echo ""
	echo " -i, --intense"
	echo "     Prints text with intense background"
	echo ""
	echo "Example"
	echo '   color red'
	echo '     echo "red text"'
	echo '   color reset'
	echo '     echo "normal text"'
	echo ""
	echo '   color red -b'
	echo '     echo "red bold text"'
	echo '   color reset'
	echo ""
	echo "  ----------------------------------------------- "
}


# get Arguments and process them for further use
# 
if [ $# -gt 0 ]; then
    if [ "$1" == "-v" ] || [ "$1" == "--version" ]; then
        echo "$VERSION"
        exit 0
    elif [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
        print_help
        exit 0
    else
        COLOR=`echo $1 | awk '{print tolower($0)}'`
        ARG=$(echo $@ |egrep -o "\-.+|--.+")
    fi
else
    color red -b
       echo "Arguments are missing"
    color reset
    echo ""
    print_help
    exit 1
fi



# get the modes the text should be displayed in
# bold 	       	:	--bold        -b
# continuous 	:	--continuous  -c
# underlined	:	--underlined  -u
# intense	    :	--intense     -i
# regular	    :	--regular     -r
# background	:	--background  -B
#
sol=$(echo $ARG|egrep -c $PATTERN_REGULAR)
if [ $sol -gt 0 ]; then
		REGULAR=1
		TYPE_CODE=0
fi
sol=$(echo $ARG|egrep -c $PATTERN_BOLD)
if [ $sol -gt 0 ]; then
		BOLD=1
		TYPE_CODE=1
fi
sol=$(echo $ARG|egrep -c $PATTERN_UNDERLINED)
if [ $sol -gt 0 ]; then
		UNDERLINED=1
		TYPE_CODE=4
fi
sol=$(echo $ARG|egrep -c $PATTERN_INTENSE)
if [ $sol -gt 0 ]; then
		INTENSE=1
fi
sol=$(echo $ARG|egrep -c $PATTERN_BACKGROUND)
if [ $sol -gt 0 ]; then
		BACKGROUND=1
fi


# define the color that is used
#
if [ "$COLOR" == "red" ]       ||   [ "$COLOR" == "r" ] ; then
	COLOR_CODE=1
elif [ "$COLOR" == "green" ]   ||   [ "$COLOR" == "g" ] ; then
	COLOR_CODE=2
elif [ "$COLOR" == "yellow" ]  ||   [ "$COLOR" == "y" ] ; then
	COLOR_CODE=3
elif [ "$COLOR" == "blue" ]    ||   [ "$COLOR" == "b" ] ; then
	COLOR_CODE=4
elif [ "$COLOR" == "purple" ]  ||   [ "$COLOR" == "p" ] ; then
	COLOR_CODE=5
elif [ "$COLOR" == "cyan" ]    ||   [ "$COLOR" == "c" ] ; then
	COLOR_CODE=6
elif [ "$COLOR" == "white" ]   ||   [ "$COLOR" == "w" ] ; then
	COLOR_CODE=7
else
	# default black
	COLOR_CODE=0
fi


# define MODE that is used
#
if [ $BACKGROUND -gt 0 ]; then
	MODE_CODE=40
elif [ $INTENSE -gt 0 ]; then
	MODE_CODE=100	
elif [ $UNDERLINED -gt 0 ] || [ $REGULAR -gt 0 ] || [ $BOLD -gt 0 ]; then
	MODE_CODE=30
elif [ $INTENSE -gt 0 ]; then
	MODE_CODE=90
fi

# Set color for the next line(s)
echo -ne "\e[${TYPE_CODE};$((MODE_CODE+$COLOR_CODE))m"
