#! /bin/bash
###############################################################################
# make.sh
# Script for installing application 
# and creating symlinks in home directory (~/) for specified files
###############################################################################

export DOTFILES_DIR="~/dotfiles"

source ~/dotfiles/bash/functions

header "Linking dotfiles into ~/"
source ~/dotfiles/install/symlinkDotfiles.sh

#header "Installing Apps"
#source ~/dotfiles/install/installer.sh
#install_all


# source bashrc file
source ~/dotfiles/bashrc
