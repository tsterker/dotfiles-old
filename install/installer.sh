#! /usr/bin/bash

###############################################################################
# autoinstall.sh
# Script for installing various programs
###############################################################################

######## Variables
dotfilesDir=~/dotfiles
backups=~/bak.dotfiles/
toInstall="
tree
xclip
vim
synapse
git
ubuntu-tweak
easystroke
"
# https://github.com/numn/bash-color/blob/master/color
#latex
# ~500 mb    ==> sudo apt-get install texlive
# ~2 GB     ==> sudo apt-get install texlive-full
#gnome-do
#ubuntu-restricted-extras
#skype
#google-chrome
#easystroke 
#python
#nodejs npm
#couchdb
#imagemagick
#apache?
#sql
#php 
#pysdm


repositories="
tualatrix/ppa
"

tasks='\n'

source ~/dotfiles/bash/functions

# installs package supplied as only parameter
function installPackage()
{
    packageName=$1
    repoName=${2:-$packageName}
    sudoPrompt

    if ! checkInstalled $packageName; then
        echo "Downloading and installing $packageName... "
        echo "----------------------------------------------------->"
        sleep 0.5
        sudo apt-get install -y $repoName
        echo "<-----------------------------------------------------"
        echo "DONE." 
    else
        echo "==> $packageName already installed."
    fi
}


function install_cmatrix()
{
    name=cmatrix
    installPackage $name
}

function install_vim()
{
    name="vim"
    repoName="vim-gnome"
    dotvimFilePath="~/dotfiles/vim"
    vimrcFilePath="~/dotfiles/vimrc"

    installPackage $name $repoName

    echo "Downloading vundle plugin manager..."
    git clone https://github.com/gmarik/vundle.git ~/dotfiles/vim/bundle/vundle

    echo -n "Press [ENTER] to start installing the bundles..."
    sleep 1
    vim +BundleInstall +qall
    #vim +BundleUpdate +qall
    #echo -n "press [ENTER]..."
    echo DONE.
}

function generateSSH()
{
    sshDir="/home/$(whoami)/.ssh"
    sshKey="id_rsa"
    sshPath=$sshDir/$sshKey

    mkdir -p $sshDir
    if [ ! -f $sshPath.pub ]; then
        echo -n "No public key yet. Creating a new one..."
        # create public key
        # -q quiet
        # -t type rsa
        # -C email
        # -f output file
        # -N passphrase (empty ^= no passphrase)
        ssh-keygen -q -t rsa -C "t.sterker@gmail.com" -f $sshPath -N ""
        echo "DONE."
    else
        echo "==> Public key already exists."
    fi

    # check if ssh connection can be established
    # ==> stderr is temporarily redirected to stdout to grep output
    echo "Trying to reach git@github.com via ssh..."
    if { ssh -vT git@github.com 2>&1 1>&3 | grep "successfully authenticated" 1>&2; } 3>&1; then
        echo "==> ssh connection already set up"
    else
        echo "==> setting up ssh connection"

        ssh-add $sshPath
        #installPackage xclip
        xclip -sel clip < $sshPath.pub
        header "PUBLIC KEY (add to https://github.com/settings/ssh)"
        echo "Title: $(uname -n)"
        cat $sshPath.pub
        echo ""
        echo "==> Public key for copied to clipboard"
        echo -n "Press [ENTER] to continue..." && read 

    fi



}

function install_git()
{
    name=git

    user_name="Tim Sterker"
    user_email="t.sterker@gmail.com"
    core_exculdesfile="/home/$(whoami)/.gitignore"
    core_editor="vim"
    merge_tool="vimdiff"

    installPackage $name

    echo -e "... user.name:\t\t$user_name"
    git config --global user.name user_name
    echo -e "... user.email:\t\t$user_email"
    git config --global user.email user_email

    echo -en "... core.excludesfile:\t$core_excludesfile"
    git config --global core.excludesfile $core_excludesfile

    echo -e "... core.editor:\t$core_editor"
    git config --global core.editor $core_editor

    echo -e "... merge.tool:\t\t$merge_tool"
    git config --global merge.tool $merge_tool

    $ git config --global color.diff auto
    $ git config --global color.status auto
    $ git config --global color.branch auto
    $ git config --global color.interactive auto


    echo "==> setting up public ssh key..."
    generateSSH
    #tasks+="* set up ssh public key for github:\n"
    #tasks+="   Https://help.github.com/articles/generating-ssh-keys\n"

}



function addRepositories()
{
    currentRepos=$(egrep -v '^#|^ *$' /etc/apt/sources.list /etc/apt/sources.list.d/*.list)

    addedNew=0

    for ppa in $repositories; do
        if ! echo $currentRepos | grep --silent $ppa; then
            echo -n "Adding repository: $ppa..."
            sleep 0.5
            sudo add-apt-repository -yqq ppa:$ppa 2> /dev/null
            addedNew=1
        fi
            #sudo add-apt-repository -yr ppa:$ppa 2> /dev/null
    done

    if [ $addedNew -eq 1 ]; then
        echo -n "Updating sources list. This may take a while..."
        sleep 0.5
        sudo apt-get update > /dev/null
        echo 'OK'
    fi
}

function install_all()
{
    sudoPrompt

    addRepositories

    for package in $toInstall; do
        echo ""
        header $package
        if ! eval "install_"$package 2> /dev/null; then
            installPackage $package
        fi
    done


    #echo "."
    #echo "."
    #echo "."
    #header "Next tasks"
    #echo -e $tasks
} 
