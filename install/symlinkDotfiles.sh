#!/bin/bash
###############################################################################
# make.sh
# Script for creating symlinks in home directory (~/) for specified files
###############################################################################

######## Variables
dir=~/dotfiles
backups=~/bak.dotfiles/

# files to create symlinks for
files="bin
bash bashrc inputrc
vimrc vim
jshintrc
gitignore"

######## Script

function showExcludedFiles(){
    # set up array of constants
    declare -A used

    self=$(basename $0)
    ignore="easystroke install README.md make.sh"

    # iterate all files and this script $dir
    for file in $self $files $ignore ; do
        used[$file]=1
    done

    unused=0
    echo "Ignored files:"
    for file in $(ls); do
        [[ ! ${used[$file]} ]] && echo -e $dir/$file
        unused=1
    done

    [[ unused -eq 0 ]] && echo None
}


cd $dir
mkdir -p $backups		# create backup directory if not exists

echo "Linked files:"
for file in $files; do

    mkdir -p $(dirname .$file)

    if [ -L ~/.$file ]; then
        if [ -d ~/.$file ]; then
            cp -r $(readlink ~/.$file) $backups > /dev/null 2>&1
        else
            cp $(readlink ~/.$file) $backups > /dev/null 2>&1
        fi
    else
        cp -r ~/.$file $backups > /dev/null 2>&1
    fi

    rm -r ~/.$file > /dev/null 2>&1

    ln -s $dir/$file ~/.$file
    echo $dir/$file
done

# SPECIAL CASE EASYSTROKE
if [ -d ~/.easystroke ]; then
    cp -r ~/.easystroke $backups > /dev/null 2>&1 
    rm -r ~/.easystroke
fi
cp -r $dir/easystroke ~/.easystroke





# show which files weren't touched
showExcludedFiles


# source bashrc file
source ~/.bashrc
